# Copyright 2023 ForgeFlow, S.L.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    "name": "Company Default Partner Pricelist",
    "summary": """
        Define default partner pricelist per company.
    """,
    "version": "13.0.1.0.1",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://github.com/OCA/partner-contact",
    "category": "Partner Management",
    "depends": ["product"],
    "data": ["views/res_company_views.xml"],
    "installable": True,
    "license": "LGPL-3",
}
