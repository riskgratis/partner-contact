To use this module simply assign a default pricelist to the companies for
which you want to have a specific default value.

If no default pricelist is assigned, the standard logic will apply.
