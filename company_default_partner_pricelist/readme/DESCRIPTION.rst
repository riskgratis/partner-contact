If the pricelist of a partner is not explicitly set, Odoo will try by default
to find a valid pricelist based on the country of the partner.

This module allows assigning a default pricelist for each company in the system
to use as the default pricelist for partners.
